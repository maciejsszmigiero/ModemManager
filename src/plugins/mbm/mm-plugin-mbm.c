/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details:
 *
 * Copyright (C) 2008 Ericsson AB
 * Copyright (C) 2012 Lanedo GmbH
 *
 * Author: Per Hallsmark <per.hallsmark@ericsson.com>
 * Author: Aleksander Morgado <aleksander@lanedo.com>
 */

#include <string.h>
#include <gmodule.h>

#define _LIBMM_INSIDE_MM
#include <libmm-glib.h>

#include "mm-log-object.h"
#include "mm-plugin-common.h"
#include "mm-broadband-modem-mbm.h"

#if defined WITH_MBIM
#include "mm-broadband-modem-mbim.h"
#endif

#define MM_TYPE_PLUGIN_MBM mm_plugin_mbm_get_type ()
MM_DEFINE_PLUGIN (MBM, mbm, Mbm)

/*****************************************************************************/

#if defined WITH_MBIM
static const struct {
    guint16 vendor;
    guint16 product;
} mbim_avoid_modems[] = {
   /* HP hs2350 (fw R3C11) no supported cell id reporting or GPS port via MBIM */
   { 0x03f0, 0x3d1d },
};
#endif

static MMBaseModem *
create_modem (MMPlugin *self,
              const gchar *uid,
              const gchar *physdev,
              const gchar **drivers,
              guint16 vendor,
              guint16 product,
              guint16 subsystem_vendor,
              GList *probes,
              GError **error)
{
    gboolean is_mbim;

#if defined WITH_MBIM
    is_mbim = mm_port_probe_list_has_mbim_port (probes);
    if (is_mbim) {
        gboolean skip_mbim = FALSE;
        size_t i;

        for (i = 0; i < G_N_ELEMENTS (mbim_avoid_modems); i++) {
            if (vendor == mbim_avoid_modems[i].vendor &&
                product == mbim_avoid_modems[i].product) {
                skip_mbim = TRUE;
                break;
            }
        }

        if (skip_mbim) {
            mm_obj_dbg (self, "Not using MBIM support in this Ericsson modem due to known issues");
        } else {
            mm_obj_dbg (self, "MBIM-powered Ericsson modem found...");
            return MM_BASE_MODEM (mm_broadband_modem_mbim_new (uid,
                                                               physdev,
                                                               drivers,
                                                               mm_plugin_get_name (self),
                                                               vendor,
                                                               product));
       }
    }
#else
    is_mbim = FALSE;
#endif

    return MM_BASE_MODEM (mm_broadband_modem_mbm_new (uid,
                                                      physdev,
                                                      drivers,
                                                      mm_plugin_get_name (self),
                                                      vendor,
                                                      product,
                                                      is_mbim));
}

/*****************************************************************************/

MM_PLUGIN_NAMED_CREATOR_SCOPE MMPlugin *
mm_plugin_create_mbm (void)
{
    static const gchar *subsystems[] = { "tty", "net", "usbmisc", NULL };
    static const gchar *udev_tags[] = {
        "ID_MM_ERICSSON_MBM",
        NULL
    };

    return MM_PLUGIN (
        g_object_new (MM_TYPE_PLUGIN_MBM,
                      MM_PLUGIN_NAME,               MM_MODULE_NAME,
                      MM_PLUGIN_ALLOWED_SUBSYSTEMS, subsystems,
                      MM_PLUGIN_ALLOWED_UDEV_TAGS,  udev_tags,
                      MM_PLUGIN_ALLOWED_AT,         TRUE,
                      MM_PLUGIN_ALLOWED_MBIM,       TRUE,
                      NULL));
}

static void
mm_plugin_mbm_init (MMPluginMbm *self)
{
}

static void
mm_plugin_mbm_class_init (MMPluginMbmClass *klass)
{
    MMPluginClass *plugin_class = MM_PLUGIN_CLASS (klass);

    plugin_class->create_modem = create_modem;
}
